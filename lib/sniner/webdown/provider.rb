require 'uri'
require 'cgi'
require_relative '../http'

module Sniner
module WebDown


module Provider
    def self.providers
        [Marky, FileReader, ZimReader]
    end

    class Marky
        ID = :marky
        DEFAULT_SERVICE = 'http://fuckyeahmarkdown.com/go/'

        attr_reader :service

        def initialize(options={})
            @service = URI.parse(options.fetch(:service, DEFAULT_SERVICE))
            @read = options.fetch(:readability, true)
        end

        def provide(url)
            if File.exist? url
                file_to_markdown(url)
            else
                url_to_markdown(url)
            end
        end

        private

        def file_to_markdown(path)
            markdownify(html: CGI.escape(File.read(path)), read: @read ? '1' : '0')
        rescue => ex
            $stderr.puts "ERROR: Unable to markdownify file '#{path}': #{ex}"
        end

        def url_to_markdown(url)
            markdownify(u: CGI.escape(url.to_s), read: @read ? '1' : '0')
        rescue => ex
            $stderr.puts "ERROR: Unable to markdownify '#{url}': #{ex}"
        end

        def markdownify(form={})
            case res = Sniner::HttpHelper.post(service, form)
            when Net::HTTPSuccess
                res.body.lines.map(&:chomp)
            end
        end
    end

    class FileReader
        ID = :file

        def initialize(options={})
        end

        def provide(path)
            f = path.respond_to?(:readlines) ? path : File.new(path)
            f.readlines.map(&:chomp)
        rescue => ex
            $stderr.puts "ERROR: Unable to read file '#{path}': #{ex}"
            []
        end
    end

    class ZimReader
        ID = :zim

        def initialize(options={})
        end

        def provide(path)
            raise "Not implemented yet."
        end
    end
end

end
end
