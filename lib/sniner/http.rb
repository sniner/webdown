module Sniner
    require 'net/http'
    require 'uri'

    module HttpHelper
        def self.get(url, retries=3)
            raise ArgumentError, 'ERROR: Too many HTTP redirects, giving up' if retries <= 0

            uri = (url.respond_to? :request_uri) ? url : URI.parse(url)
            Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme=='https') do |http|
                req = Net::HTTP::Get.new(uri)
                res = http.request(req)
                case res
                when Net::HTTPSuccess
                    res
                when Net::HTTPRedirection
                    prev = uri
                    uri = URI.parse(res['Location'])
                    $stderr.puts "REDIRECT: #{prev} -> #{uri}"
                    HttpHelper.get(uri, retries-1)
                else
                    $stderr.puts "ERROR: Unable to get '#{uri}': code = #{res.code}"
                    nil
                end
            end
        end

        def self.get_data(url)
            uri = (url.respond_to? :request_uri) ? url : URI.parse(url)
            Net::HTTP.get(uri)
        rescue
            nil
        end

        def self.post(url, form={}, body=nil)
            uri = (url.respond_to? :request_uri) ? url : URI.parse(url)
            retries = 3
            while retries>0 do
                Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme=='https') do |http|
                    req = Net::HTTP::Post.new(uri)
                    req.set_form_data(form)
                    req.body = body if body
                    res = http.request(req)
                    case res
                    when Net::HTTPRedirection
                        prev = uri
                        uri = URI.parse(res['Location'])
                        $stderr.puts "REDIRECT: #{prev} -> #{uri}"
                        retries -= 1
                    when Net::HTTPSuccess
                        return res
                    else
                        $stderr.puts "ERROR: Unable to post '#{uri}': code = #{res.code}"
                        break
                    end
                end
            end
            nil
        end

    end
end

# vim: set ts=4 sw=4 et: 
