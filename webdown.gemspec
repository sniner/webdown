Gem::Specification.new do |s|
    s.required_ruby_version = '>= 2.1'
    s.name        = 'webdown'
    s.version     = '0.1.2'
    s.date        = '2018-02-04'
    s.summary     = 'Markdown web clipping tool'
    s.description = 'Commandline tool for markdown conversion via fuckyeahmarkdown.com'
    s.authors     = ['Stefan Schönberger']
    s.email       = ['mail@sniner.net']
    s.homepage    = 'https://github.com/sniner/marky-cli'
    s.license     = 'GPL-3.0'
    s.files       = `git ls-files`.split("\n").reject {|f| File.dirname(f)=='.'}
    s.executables = Dir.glob('bin/*').map {|f| File.basename(f)}
end

# vim: set et sw=4 ts=4:
