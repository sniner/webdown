# Download webpages as Markdown files

You like Markdown? Me too. Until recently I used Evernote for notes, but tried to switch to [Zim][zim] and a [syncing tool][sync] for some reasons. Zim didn't make me happy, because I do not want to memorize a different markup syntax - once more. I want to stick with GHF Markdown or [CommonMark][cm]. So I ditched Zim for pure Markdown files in a git repository.

There was one thing missing: a decent webclipping utility for Markdown. There is a great [web service][fymd] that does the job, but I wanted

* a CLI tool
* download of resources (e.g. illustrations) and changing links to these downloaded resources
* fixing some glitches
* *TODO*: converting Zim markup to CommonMark

That's why I came up with this tool: `webdown`

## Installation

`webdown` is a Ruby gem. After downloading the source folder just build and install it:

```
$ gem build webdown.gemspec
$ gem install webdown-0.1.0.gem
```

Please follow the instructions of your distribution on how to add Ruby gems to your execution path.

## Usage

`webdown` uses a two-step process: a **content provider** and some **post-processors** that transforms the content. Currently there are two content providers: a web service that converts web pages to markdown and a simple file reader for converting or postprocessing local files. You may choose to omit postprocessing.

> **Attention**: Please remember, this tool uses a web service to convert a web site into Markdown! The provider of this web service may store all links to web pages you convert. If you convert local files, the content will be uploaded to the web service. I do NOT run this web service and I am not responsible for what it does.

When given a URL, for example like that:

```
$ webdown http://example.com/text.html
```

`webdown` will contact the [web service][fymd] for conversion and afterwards postprocess the Markdown content and show the text. Because there is no output file specified, `webdown` will not download resource files.

So, let's add an output file name:

```
$ webdown http://example.com/text.html -s -o text.md
```

Now, the Markdown content will be written to `text.md` and because of option `-s` a folder named `text` will be created that contains all pictures that are referenced in `text.md`.

You may keep resources of many Markdown files in one directory. Or have one directory for each file. It's just a matter of personal taste and the options you provide:

* `-s` will create a sidecar directory for each Markdown file
* without option all resources will be downloaded into a folder `./files`
* with `-r` you can provide a directory of your choice

To postprocess an existing Markdown file:

```
$ webdown -o text1.md text.md
```

To convert and postprocess an existing HTML file you have to specify the content provider explicitly:

```
$ webdown -o text.md -p marky text.html
```

And if you prefer to omit postprocessing:

```
$ webdown -o text.md -p marky --no-pp text.html
```

Of course you will get a short description of command options with `--help`:

```
$ webdown --help
```


[zim]: http://zim-wiki.org
[sync]: https://syncthing.net/
[cm]: http://commonmark.org/
[fymd]: http://fuckyeahmarkdown.com/

